# MASC

The campaign with MASC data are 

MASC data are usually located in the subfolder `/MASC` of the `<campaign_name>` folder `/ltedata/<campaign_name>/MASC`.

Each `/MASC` folder contain the following subfolders: 

`/Info` contains metadata on the campaign

`/Raw_data`  
- Data hierarchy: `<YYYY.MM.DD>/<HH>/<YYYY.MM.DD_HH.MM.SS>_flake_<number>_cam_<0|1|2>.png`

`/Proc_data`  
- Data hierarchy: `<YYYY.MM.DD>/<HH>/<YYYY.MM.DD_HH.MM.SS>_flake_<number>_cam_<0|1|2>.<mat|png>`

`/Quicklooks`contains graphs automatically generated during the campaign.
- Data hierarchy: `<YYYY_MM>/<YYYYMMDD>_quicklook#<1|2>.png`

<br></br>
 