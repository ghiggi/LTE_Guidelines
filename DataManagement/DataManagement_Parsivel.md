### Parsivel Data Management

This document summarizes how Parsivel data should be saved and documented on `/ltedata`. 
Parsivel data are located in the subfolder `/Parsivel` of the `<campaign_name>` folder: `/ltedata/<campaign_name>/Parsivel/`.
 
Each `/Parsivel` folder contains the following subfolders: 

 