# PolXBear Data Management
This document summarises how PolXBear data should be saved and documented on ltedata. 

Radar data are located in the subfolder `/Radar` of the `<campaign_name>` folder: `/ltedata/<campaign_name>/Radar/`.

Each `/Radar` folder contains the following subfolders:

`/Info` contains metadata of the campaign
- `<campaign_name>_campaign.log` is written during the campaign and contains the exact geographical coordinates of the radar and the details to access the radar computer. It also contains one entry per day when work was done on the radar, describing who participated and what was done. 
- `<Campaign_name>_campaign_observation_notes.txt` is written during the campaign and contains all weather-related observations.
- `log_PolXBear_sequence.txt` is written during the campaign and describes which pacsi files were launched and when they were started and stopped.

`/Pacsi_config` contains all the pacsi files used during the campaign.

`/Log_details` contains relevant parts of `xpold.log` file during which there was a crash. These files can be created by typing `tail -n200 xpold.log > xpold_<yyyymmddhhmm>.log` in the terminal.

`/Graphs` contains graphs automatically generated during the campaign. 
- Data hierarchy: 

`/Quicklooks` contains graphs automatically generated during the campaign. 
- Data hierarchy: `/quicklooks/<yyyy>-<mm>-<dd>/XPOL<datetime>scan_type.png`

`/Pictures`contains pictures from the webcam of the radar.

`/Raw_data` contains all the raw data. 
- Data hierarchy: `/Raw_data/<yyyy>/<mm>/<dd>/XPOL-<yyyymmdd>-<hhmmss>.dat`

`/Proc_data` contains the processed netCDF files.  
- Polarimetric data: `/Proc_data/<yyyy>/<mm>/<dd>/PolXBear-polar-<yyyymmdd>-<hhmmss>-<scantype>-<angle>.nc`
- FFT spectra files: `/Proc_data/<yyyy>/<mm>/<dd>/SPECTRA` 
- Graphs of processed data: `/Proc_data/Proc_data_Graphs/`
