\documentclass[a4paper,11pt]{article}

\usepackage{natbib}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}
\usepackage{url}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{soul}
\usepackage{color}
\usepackage{hyperref} % for hyperlinks
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    citecolor=blue,
} % changes colours hyperlinks

% A4
% Vertical
\voffset=-0.5cm
\topmargin=0cm
\headheight=0cm
\headsep=0cm
\footskip=1.5cm
\textheight=23.5cm
% Horizontal
\hoffset=-0.5cm
\oddsidemargin=0.0cm
\evensidemargin=0.0cm
\textwidth=17cm

\title{Field notes on the use of MXPol.  }
\author{ Jacopo Grazioli - Alexis Berne  \\ \\
\small
\texttt{jacopo.grazioli(alexis.berne)@epfl.ch} \\
\small
\texttt{Mobile: +41 78 953 87 53}\\
\small
\texttt{Office Grazioli: +41 21 693 80 72 }\\
\small
\texttt{Office Berne: +41 21 693 80 51 }\\
\'Ecole Polytechnique F\'ed\'erale de Lausanne \\
\small
Environmental Remote Sensing Laboratory
}

\begin{document}

\maketitle

\section{Power up the radar}

\subsection{Grid Power}

In normal circumstances the MXPol radar should receive power from the grid directly. This is the most safe configuration, and it should be applied whenever possible.
Make sure that the external power plug showed in the image is properly connected both in the side of the radar pod and to the grid power. 

\centerline{
\includegraphics[scale=0.4]{./figures/power_plug.jpg}
}


\section{Start all the electrical connections}

When the radar receives power, open the white pod. If everything was shut down properly you should power up the radar by following these steps:

\begin{enumerate}
	\item Check if the two power strips in the picture below are turned on. If not, turn them on.
	\item Check if the black APC back-UPS (situated in front of the radar computer) is turned on. If not, turn it on.
	\item Check if the computer is on (for example, by trying to open the CD slot). The computer should be started automatically by the UPS, when the UPS is started.
		  If it is not the case, start the computer manually.		
\end{enumerate}

\centerline{
\includegraphics[scale=0.4]{./figures/power_strip.jpg}
}


\section{How to connect to the radar}
In order to communicate with MXPol there are three ways:

\begin{enumerate}
	\item Remotely, through internet, when the connection is available. 
	\item On site, connecting the radar computer inside the white pod to a keyboard, a mouse and a monitor.
	\item On site, by connecting to the network of a 3G router (wifi {\bf PASSWORD: Teledetec-Env015}), installed inside the radar pod.
\end{enumerate}

The second solution is usually applied when there is some evident connection issue with the other methods. Be careful when this solution is applied to the movements of the antenna above the opened pod. On site, the best solution is the third one.\\

\subsection{Connection on site with a laptop}

Connect to the network of the router, by RJ45 or by WIfi. Some informations about the router are:
\vspace{0.5cm}
\centerline{IP: 192.168.5.1}
\centerline{username (if needed): admin}
\centerline{PASSWORD (wifi and setup page): Teledetec-Env015}
\vspace{0.5cm}

From a terminal type:\\

\centerline{ssh -X radarop@192.168.5.150} 
\centerline{Password: rdr-lte-012}


\subsection{Connection through internet}
 $ssh$ to MXPol as:\\

\centerline{ssh -X radarop@ltexpol.dnsalias.org} 
\centerline{Password: rdr-lte-012}

This connection is relatively slow but it allow to operate all the main actions, except for the transfer of big data.

 
\subsection{Connection on site directly from the MXPol computer}
In case any other connection type would not be available, it is possible to connect a monitor, a keyboard and a mouse to the internal computer of the MXPol. 
The login informations for MXPol are:\\

\centerline{Username: radarop} 
\centerline{Password: rdr-lte-012}

\section{How to start the MXPol computer through IPMI client}

In case the MXPol computer is turned off, but you can still connect to the router, it is possible to restart the computer through the IPMI card. NOTE: this is only possible from a LTE computer (Only IP addresses in the range 128.178.90.* are allowed to connect to the VNC ports (IPMI of the radar)).\\

If not already installed, get the IPMI software here: \url{ftp://ftp.supermicro.com/utility/IPMIView/}\\

Startup the IPMI client with \textit{IPMIView20.jar}\\

Either connect to the MXPol computer if you have it already in the IPMI Domain list or use the \textit{add a new system} button in the top left corner of the window. NOTE: you will have to update the IP address of the MXPol computer regularly! You can check the current IP address by typing the following in your terminal:

\begin{center}
nslookup ltexpol.dnsalias.org
\end{center}

Double click on the newly created IPMI Domain and login with the following credentials:\\

\begin{center}
Login ID: ADMIN\\
Password: lte-rad-015
\end{center} 

In the \textit{IPM Device} tab, click the \textit{Power Up} button.

\section{Operate the radar}

\subsection{Start the radar software}
All the software startup operations are preferably conducted remotely from people at LTE. In case this would not be  possible, this section explains how to deal with the software.\\


\noindent
Once logged into the computer of MXPol, it is necessary to start the the radar software, called {\it xpold}.
At the login you should be in the directory {\it /home/radarop/}. If not, go to that directory.
At first, check if {\it xpold} is already running. Type:

\centerline{ps -u radarop}

\noindent
This will give you the list of processes running with ``radarop'' as user, as shown here below. 

\centerline{
\includegraphics[scale=0.4]{./figures/processes.png}
}

\noindent
In this case {\it xpold} is running with PID 3574. If you want to kill it, before to restart, type:

\centerline{kill -9 3574} 

\noindent
The software itself is started with the command:

\centerline{xpold \&} 

\noindent
It can be launched also with additional options, the most important ones being $-g$ and $-p$ which exclude the GPS and the pedestal movement, respectively.
As an example, the command:

\centerline{xpold -g \&} 

\noindent
is used to excludes the GPS. It can be useful when the GPS is faulty or there is no coverage. 
\underline{Note that the radar will not work properly if the GPS is faulty and the $-g$ option is not used.}

\subsection{Basic startup operations}

Once $xpold$ is running, it is possible to communicate with the radar through a Graphical User Interface (GUI).
To start the GUI, type:

\centerline{xpol{\_}control \&} 

This will open the following window. Now all the startup operations can be conducted from here.
\centerline{
\includegraphics[scale=0.5]{./figures/GUI.png}
}
 

First of all, click the ``Connect'' button. Now you will se on the top righ corner of the GUI the information about the actual antenna position,
namely:
\begin{itemize}
	\item {\it Position Az}: Azimuth     [$^\circ$]
	\item {\it Position El}: Elevation   [$^\circ$] 
	\item {\it Velocity Az}: Azimuth istantaneous speed [$^\circ$~s$^{-1}$ ]
	\item {\it Velocity El}: Elevation istantaneous speed [$^\circ$~s$^{-1}$ ]
\end{itemize}

We will now move the antenna, until it points vertically. To do so:

\begin{enumerate}
	\item Under the scrolling menu {\it Action}, choose ``Point''
	\item Under the interactive fields ``Position'': put 0 for Az, and 90 for El.
	\item Under the interactive fields ``Velocity'': put 20 for Az, and 20 for El.
	\item Click on the ``Apply'' button in the top-right corner of the GUI.
\end{enumerate}

The antenna will move to the selected verical position of 0$^\circ$ Azimuth and 90$^\circ$ Elevation.
Up to now we used only the options in the upper right corner of the GUI, which control the mechanical movements of the antenna.
We will focus now on the left side of the GUI, which allows to set up the generation of electromagnetic pulses, and start to transmit in the atmosphere.
The operations to conduct are the following:

\begin{enumerate}
	\item Check the box named ``RF power''. This will give power to the magnetron which will start to warm up.
	\item Wait 3-4 minutes to ensure that the magnetron is properly powered
	\item Set ``Mode'' to ``Pulse pair''. It is the simplest transmission mode, suitable for startup of the radar.
	\item Set ``Range resolution'' and ``Range gate spacing''  to ``75 m''
	\item Set ``PRI 1,2'' and ``Group interval'' to 1000, 1000, and 1400 respectively
	\item Set ``Filter bandwidth'' to 20 MHz
	\item Set ``Frequency tracking'' to ``Manual''
	\item Type 20 in the field named ``LO frequency''
	\item Set ``Server state'' to 'Run'. This will start the transmission in the atmosphere. Use ``idle'' instead if you don't want to transmit
	\item Click on the ``Apply'' button at the bottom of the GUI. This will apply all the present settings to MXPol.
	\item \underline{Here sometimes the system can crash.} This can happen because the magnetron is not warm enough or there is not GPS reception. If you experience a crash, you will have to kill {\it xpold} and start the operation again.
	\item If there is no crash of {\it xpold}, you will have to focus your attention on the field named ``Power''. Wait until the power takes values different than 0. When the displayed power is ``nan'' it means that the radar is not able to track the transmitted frequency. Good values of power instead range between -14 dBm and -20 dBm. The higher the better.
	\item Wait 1 minute. If the power is still ``nan'' click on ``Apply'' another time, and wait again for a maximum of 2-3 minutes.
	\item Now set ``Frequency tracking'' to ``Correct'', and click on ``Apply''
	\item The goal now is to gradually lower the ``Filter bandwidth'' until 5 MHz. Set it to 10 MHz, click apply and wait 30 seconds. Then again, set to 5 MHz. If everything works fine, in the end you will see that the ``Power'' takes values between -14 dBm and -20 dBm. \underline{If the power is always equal to 0}, try to repeat the process. If it doesn't help, call for assistance. \hl{Note that many of the configurations of the past steps can be obtained faster by ``Loading'' (Load button in the top left of the GUI) the ``warmup'' configuration file located in the '/home/radarop/pax\_files' folder}
\end{enumerate}

At this stage the radar is transmitting and the antenna points vertically. We are not recording any data. In order to start to record we will have to load a ``Pacsi'' file, which is a set of commands that MXPol will interpret and execute.
To load a Pacsi file:
\begin{enumerate}
	\item Set ``Action'' to ``Load Pacsi file''
	\item A new window will open, asking to select a Pacsi file to load. \\
	The files are stored in {\it /home/radarop/pax{\_}files/}. Choose the proper file. 
	\item In the GUI, click the ``Apply'' button in the top right corner.
\end{enumerate}

The antenna will soon start to turn, following the scanning protocol provided by the Pacsi file. You can now log out from the computer of MXPol.  




\subsection{Data Storage}
When the radar is collecting data, they are copied locally into two external hard drives situated inside the white pod, and connected to the radar computer through USB.
Make sure that the hard disks are powered and properly connected to the radar computer.\\

\noindent
In normal circumstancies the hard drives should be already mounted in the filesystem of the radar computer. In case of reboot or power failure, the two units needs to be manually mounted. To check if they are mounted, explore the directory $/storage/disk1$ and $/storage/disk2$, using the list ($ls$) command:\\

\centerline{$ls\, /storage/disk1$} 

\noindent
In case these directory are empty, you will need to mount them. At first, find the proper devices:\\

\centerline{$ls\, /dev/sd*$}

Ignore all the $sda$ devices, like $sda$, $sda1$, etc. You should see $sdb1$ and $sdc1$. These are the device to be mounted.
To do so:\\

\centerline{$sudo\,\, mount /dev/sdb1 /storage/disk1$}
\centerline{$sudo\,\, mount /dev/sdc1 /storage/disk2$}

Now check again the two storage device in the respective directories, and they should be properly recognized.



 





\end{document}
