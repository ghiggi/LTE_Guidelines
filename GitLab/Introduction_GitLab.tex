\documentclass{article}

\usepackage{amsmath} % for  math
\usepackage{bigints} % for big integral signs
\usepackage{fixltx2e} % for subscript in text

\usepackage{latexsym}

\usepackage{graphicx} % for pics
\usepackage{adjustbox}
\usepackage{subfig}
\usepackage{float}
\usepackage{wrapfig}
\restylefloat{figure}

\usepackage{capt-of} % large captions

\usepackage{hyperref} % for hyperlinks
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    citecolor=blue,
} % changes colours hyperlinks

% avoids hyperref in table of contents:

%\makeatletter
%\let\Hy@linktoc\Hy@linktoc@none
%\makeatother

% adding [hidelinks] in documentclass hides ugly colours and boxes around hyperlinks; i.e. \documentclass[hidelinks]{article}

\usepackage{booktabs} % for toprule, midrule, bottomrule
\usepackage{xcolor}
\usepackage[margin=0.9in]{geometry}
\definecolor{mygray}{gray}{0.6}
%\usepackage{lscape}
\usepackage{array} % for tables
\usepackage{ragged2e} % for text alignment
\usepackage{color, colortbl}
\definecolor{LightCyan}{rgb}{0.88,1,1}

\usepackage[labelsep=space]{caption}

\usepackage{listings}

% for code in text:

\usepackage{courier}

\definecolor{codegreen}{rgb}{0,0.6,0}
\definecolor{codegray}{rgb}{0.5,0.5,0.5}
\definecolor{codepurple}{rgb}{0.58,0,0.82}
\definecolor{backcolour}{rgb}{0.95,0.95,0.92}
 
\lstdefinestyle{bashinputstyle}{
    backgroundcolor=\color{backcolour},   
    commentstyle=\color{codegreen},
    keywordstyle=\color{magenta},
    numberstyle=\tiny\color{codegray},
    stringstyle=\color{codepurple},
    basicstyle=\small\ttfamily,breaklines=true,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                                 
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2,
    xleftmargin = 2cm,
    framexleftmargin = 1em,
    literate={\$}{{\textcolor{codegreen}{\$}}}1
}

 
\lstset{style=bashinputstyle}

% for using csv files

\usepackage{csvsimple}

 
\begin{document}
\pagenumbering{gobble}

\hfill \break
\hfill \break

\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.25]{/home/fvanden/Documents/LaTex/WeeklyReport/EPFL.png}
\end{figure}

\begin{center}
\LARGE \textbf{Introduction to Git Lab}\\
\hfill \break
\Large LTE, EPFL, Switzerland\\
\hfill \break
April, 2016 

\hfill \break

\noindent \large \textbf{Document History}
\end{center}

\normalsize

\begin{table}[h!]
	\begin{center}
		\begin{tabular}{|l|l|p{10cm}|}
			\hline \textbf{Version} & \textbf{Date} & \textbf{Activity}\\
			\hline 0.0 & 26.04.2016 & Created by Floor van den Heuvel\\

			\hline
		\end{tabular}
	\end{center}
\end{table}

\newpage


{\hypersetup{linkcolor=black}

\tableofcontents
}

\newpage

\pagenumbering{arabic}

\section{Why Git Lab?}

Git is a source control system, which means that it can be used to manage a directory tree in which for example source code, documentation or other files are stored. This is called the repository. In this report a distinction is made between Git (the software) and GitLab (the graphical user interface). Working with Git has the following advantages:

\begin{itemize}
\item Files can be changed from multiple computers, by multiple users and shared easily (no need to send files by email).
\item For GitLab specifically, different types of rights can be assigned to different users, so that not everybody who works on the repository can also make changes to the master branch. This allows you to keep a clean and stable version of your work. Ownership and access rights will be discussed in section \ref{Ownership}.
\item For all files, changes are tracked automatically and previous versions off all the files are stored. Meaning that you will be able to reproduce that plot from this publication or that poster, even though your code may have developed since then.
\item Each change is given a code (a hash) with a time stamp, information on the changes and on the user who made the changes.
\item The repository is stored on each user's computer, as well as in a central location (for EPFL, this is the git.epfl.ch server), providing a reliable backup.  
\item The graphical interface GitLab is particularly useful for merging branches (that is, comparing different versions of the same repository before putting these together in one final version, as will become clear in section \ref{Branching}.
\end{itemize}

\begin{wrapfigure}{l}{0.5\textwidth}
	\centering
	\includegraphics[scale = 0.5]{git-transport.png}
\end{wrapfigure}  

Git uses three main operations: 

\begin{itemize}
\item[] \textbf{add}: adds new/changed files to a commit
\item[] \textbf{commit}: finalizes the commit locally, storing it in the local repository (which is on the user's computer)
\item[] \textbf{push}: copies the commits from the local repository on the remote repository
\end{itemize} 

It is also possible to branch the workflow, that is, to create multiple versions of the file tree that exist in parallel. The advantage is that the master branch is maintained as a clean and stable version of the repository while the work-in-progress version is in a separate branch. Branching will be discussed in more detail in section \ref{Branching}.  

\clearpage
\newpage

\section{Installing Git Lab and getting started}

This section will guide you trough the installation of Git on your computer, the creation of a Git repository, adding files, conflict resolution and some notions of branching. Many of the steps here have been taken from, and are explained in more detail in the EPFL Git tutorial: \url{http://wiki.epfl.ch/git-epfl/documents/git.html#setup-git}. Also, many more useful commands can be found in this tutorial!

\subsection{Installing GIT client}

\subsubsection{Windows}

Download and install Git from: \url{http://git-scm.com/download/win}. An easy way to get Git installed is by installing GitHub for windows: \url{http://windows.github.com}.

\subsubsection{Mac}

Download and install Git from: \url{http://git-scm.com/download/mac}

\subsubsection{Linux}

Under Debian/Ubuntu/Mint:\\

\begin{lstlisting}[language=bash]
$ sudo apt-get install git
\end{lstlisting}

\subsection{Git global set up}

For Linux users, open a terminal. For For windows you will need a Git bash application which will accept Unix-like Git compatible options. For the global set up, enter:

\begin{lstlisting}[language=bash]
$ git config --global user.name "user name"
$ git config --global user.email "user.name@epfl.ch"
\end{lstlisting}

You can check your settings with:

\begin{lstlisting}[language=bash]
$ git config --list
\end{lstlisting}

or specific settings:

\begin{lstlisting}[language=bash]
$ git config user.name
\end{lstlisting}

Note: After creating a repository, GitLab (only) will prompt you to enter an SHH key in order to establish a secure connection between your computer and GitLab. This is really straightforward and GitLab will guide you through it. Without adding the SSH key it is impossible to push commits or pull the Git repository. 

\subsection{Creating your own EPFL-Git repository}

\begin{enumerate}
\item To create your repository either go to \url{https://git.epfl.ch} (hereafter git) or to \url{https://gitlab.epfl.ch} (hereafter gitlab). The main difference between the two is the graphical user interface, but the command line instructions are the same.  

\item For git, click on "create a new repository" and follow the instructions. For gitlab, click on the $+$ symbol in the upper right corner of the page and follow the instructions. Note that it is also possible to import a project from another Git repository.

\item In the terminal, you can clone the repository by entering for git:

\begin{lstlisting}[language=bash]
$ git clone https://gaspar@git.epfl.ch/repo/<name-of-your-choice>.git
\end{lstlisting}

and for gitlab:

\begin{lstlisting}[language=bash]
$ git clone git@gitlab.epfl.ch:gaspar/<name-of-your-choice>.git
\end{lstlisting}

Note: the repository URL and the command for cloning the repository are given on the repository page in both git and gitlab.

\item a directory with the same name as the git repository will have appeared on your local drive, together with any files which were in the git repository and a folder called ".git".

\item to add a file to your repository, create a new file in the directory on your local drive (i.e. test.txt)

\item add this file to your Git repository by entering:

\begin{lstlisting}[language=bash]
$ git add test.txt
\end{lstlisting}
     
\item commit your changes:

\begin{lstlisting}[language=bash]
$ git commit test.text
\end{lstlisting}

or:

\begin{lstlisting}[language=bash]
$ git commit test.text -m"message with commit"
\end{lstlisting}

\item and push your changes to your Git remote repository

\begin{lstlisting}[language=bash]
$ git push origin master
\end{lstlisting}

\end{enumerate}

\subsection{Conflict resolution}

When working from multiple machines or collaborating within a project, it is possible that changes made by one user conflict with changes made by another user... i.e. ref\_biblio on the commun1!! The best solution in this case would be to branch the workflow (see the next section), however, if for some reason you have not branched the workflow here is a more correct way of committing files to the Git repository:

\begin{enumerate}
\item add files
\item commit files
\item before pushing the changes you've made locally, pull to get the server changes:

\begin{lstlisting}[language=bash]
$ git pull
\end{lstlisting}

\item if changes have been applied to different lines in the document, no conflicts will appear. However, if the same line has been changed in both versions, there is no unique merge solution and you will get a conflict message. The file with the conflicting lines will reappear in your local folder with the local version and the server version in brackets:  

\begin{lstlisting}
<<<<<<<<<<
server version
==========
local version
>>>>>>>>>>
\end{lstlisting}

\item open the conflicting file and choose one of the versions by removing anything else than what you want to keep in the file.

\item finish the conflict resolution:

\begin{lstlisting}[language=bash]
$ git commit -a
\end{lstlisting}

\end{enumerate}

The conflict resolution will be represented differently in the gitlab commits list:

\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.35]{Commits_Conflict(2).png}
\end{figure}

Clicking on the commit will show the details of the conflict resolution:

\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.3]{Commit_Conflict_detail.png}
\end{figure}  

\subsection{Branching}\label{Branching}

When collaborating on a project or sharing your repository with someone else, it is a better idea to keep a clean, stable version of your code in the master branch and to use a different, development branch (i.e. yourname-devel) to work on. You can display existing local and remote branches using:

\begin{lstlisting}[language=bash]
$ git branch -a
\end{lstlisting}

This will give the following output:

\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.5]{branching_terminal.png}
\end{figure}
	
To create a new branch and switch to it:

\begin{lstlisting}[language=bash]
$ git checkout -b the_branch_name:
\end{lstlisting}

or to switch to another, existing branch:

\begin{lstlisting}[language=bash]
$ git checkout the_branch_name
\end{lstlisting}

Again, see the \href{http://wiki.epfl.ch/git-epfl/documents/git.html#setup-git}{EPFL Git tutorial} for more options.\\

In gitlab, branching is made a lot easier with the graphical user interface:

\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.35]{branching(2).png}
\end{figure}
	
\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.3]{Branch_create.png}
\end{figure}

The branch is then displayed in the same way as the master, including the commits from the master at the time that the branch was created:

\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.3]{branch_display.png}
\end{figure}

It is possible to compare the master with the developer branch:

\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.3]{branch_compare.png}
\end{figure}
 
And to create a merge request which will merge the master branch and the developer branch, or two developer branches:

\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.3]{branch_merge.png}
\end{figure}

This merge request can be assigned to a specific person, or, according to the ownership rights, will be only assignable to the owner of the project, who will have to either accept or reject the request.\\

The merge request and the changes can be reviewed before accepting the request:

\begin{figure}[H]
	\centering
	\includegraphics[scale = 0.3]{merge_request.png}
\end{figure} 

This is, of course, also possible directly from the command line: 

\begin{lstlisting}[language=bash]
$ git checkout master
$ git pull --rebase origin master
$ git merge floor-devel
\end{lstlisting}

The first line switches to the master branch, the second line pulls changes from the remote master to the local master and the third line merges the developer branch with the master branch.\\

If the master branch has changed since the developer branch was created and conflicting lines exist, this will be displayed in the following way:

\begin{lstlisting}
<<<<<<< HEAD
I changed this line in the master
=======
I added this line in the developer branch
>>>>>>> floor-devel
\end{lstlisting}

The resolution is somewhat similar to conflict resolution described in the previous section. For a more detailed description on how to solve merge requests from the command line, see the \href{http://wiki.epfl.ch/git-epfl/documents/git.html#setup-git}{EPFL Git tutorial}.


\newpage

\section{Recommendations for use and ownership at LTE}\label{Ownership}

GitLab allows to have several projects and users within a group:

\begin{itemize}
\item A group is a collection of several projects
\item Groups are private by default
\item Members of a group may only view projects they have permission to access
\item Group project URLs are prefixed with the group namespace (i.e. https://gitlab.epfl.ch/groupname/projectname) 
\item Existing projects may be transferred into a group
\end{itemize}

A benefit of putting multiple projects into a group is that you can give a new user access to all the projects in the group with only one action. Only the group owner can add members to a group, and when adding a member the group owner decides the access rights of the new member. There are 5 types of access rights:

\begin{enumerate}
\item Guest - No access to code
\item Reporter - Read the repository
\item Developer - Read/Write to the repository
\item Master - Read/Write to the repository $+$ partial administrative capabilities
\item Owner - Read/Write to the repository $+$ full administrative capabilities 
\end{enumerate}

\textit{Guests} for example, can only see the issues in the project, but do not have access to the code or files.\\

\textit{Reporters} on the other hand, have the same abilities as guests, but also have reading access to the code, meaning that they can fork the project. This kind of access is ideal for persons who you do not want to be pushing to your repository but still want to give access to the code or files. \textit{Examples could be; students, collaborators outside of LTE involved in the project but not working directly on codes.}\\

\textit{Developers} have write permission to the repository, meaning that they can branch off, work in their own branch and push that branch back to the repository. \textit{For example; all LTE members, collaborators outside of LTE working directly on the codes.}\\

\textit{Masters} and \textit{Owners} have the same access rights as developers but also have various administrative capabilities. \textit{For example, designated LTE members who are responsible for the group and Alexis.}\\

NOTE: it is possible to change group ownership and to have multiple owners. Changing project ownership is possible but more complicated.\\

Table \ref{table:1} shows the permissions in more detail for group members, and table \ref{table:2} for project members. If a member is added to the group with developer rights, this member automatically gets 'developer' access to all the projects within the group. However, it is possible to increase the access level of an individual user for a specific project.\\

\begin{table}[h]
\centering
\caption{\label{table:1}Group ownership rights} 
\csvautotabular{GroupOwnership.csv}
\end{table} 

\begin{table}[h]
\centering
\caption{\label{table:2}Project ownership rights} 
\csvautotabular{ProjectOwnership.csv}
\end{table}

\clearpage
\newpage

\section{Useful commands and workarounds}

In this section, some specific solutions and useful GIT commands can be added and listed.

\subsection{using gitignore for the frequent exclusion of specific files and file types (fvanden 26.04.2016)} 

Often, instead of adding and committing files one at the time, you will be adding whole folders or all files in a folder, using for example:

\begin{lstlisting}[language=bash]
$ git add *
\end{lstlisting}

\begin{lstlisting}[language=bash]
$ git commit -m"commit message"
\end{lstlisting}

Sometimes, there are some types of files which you will want to be ignored by the commit (i.e. .pyc files), you could, for example, use:

\begin{lstlisting}[language=bash]
$ git reset HEAD path/to/file
\end{lstlisting}

to undo a git add before git commit. However, if there are many files you need to remove, this can become very tedious, especially if you're committing to your GIT repository several times a day! A better option is to add a ".gitignore" file to your ".git" folder (in the cloned folder on your local drive). In this .gitignore file, you can list filenames or file extensions that GIT should always ignore, for example:

\begin{lstlisting}[language=bash, caption = .gitignore]
.pyc
.png
\end{lstlisting} 

\newpage 



\end{document}