\contentsline {section}{\numberline {1}Why Git Lab?}{1}{section.1}
\contentsline {section}{\numberline {2}Installing Git Lab and getting started}{2}{section.2}
\contentsline {subsection}{\numberline {2.1}Installing GIT client}{2}{subsection.2.1}
\contentsline {subsubsection}{\numberline {2.1.1}Windows}{2}{subsubsection.2.1.1}
\contentsline {subsubsection}{\numberline {2.1.2}Mac}{2}{subsubsection.2.1.2}
\contentsline {subsubsection}{\numberline {2.1.3}Linux}{2}{subsubsection.2.1.3}
\contentsline {subsection}{\numberline {2.2}Git global set up}{2}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Creating your own EPFL-Git repository}{2}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Conflict resolution}{3}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Branching}{4}{subsection.2.5}
\contentsline {section}{\numberline {3}Recommendations for use and ownership at LTE}{8}{section.3}
\contentsline {section}{\numberline {4}Useful commands and workarounds}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}using gitignore for the frequent exclusion of specific files and file types (fvanden 26.04.2016)}{10}{subsection.4.1}
