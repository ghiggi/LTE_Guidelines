## Welcome to LTE !

Congratulations on becoming our newest group member! 
This document gives you an overview of how our lab works and how we organize our research.
For additional informations regarding EPFL, please check the document [Welcome to EPFL](WelcomeEPFL.md).

### People to contact:


| Position             | Name                  | E-mail                         | Subjects                                                                              |
|----------------------|-----------------------|--------------------------------|---------------------------------------------------------------------------------------|
| Secretary            | Magdalena Schauenberg | magdalena.schauenberg@epfl.ch  | HR questions, contract, annual leave, illness, travel requests, travel reimbursements |
| System administrator | Denis Rochat          | denis.rochat@epfl.ch           | Technical issues with lab computers / servers, computer equipment                     |
| Computer services    | Samuel Bancal         | samuel.bancal@epfl.ch          | Software in ENAC computer rooms for TA sessions or more general infos on Linux        |
| ENAC secretary       | Emma Sorrentino       | emma.sorrentino@epfl.ch        | Doctoral school related questions and administration                                  |


### LTE Computing Servers:

There are currently 6 servers at LTE, that you can use to run your codes. 
You should log in using your personal username. 
Login as `lteuser` should be performed only for specific reasons (e.g. upload campaign data on `/ltedata`, see next section).
As a rule of thumb, use your personal username to access the servers ! 

You can log in to the server from any terminal as follow:
- `ssh <myusername>@<servername>.epfl.ch`
- `ssh lteuser@ltesrv5.epfl.ch`.

Ask Denis or anyone from the lab for the lteuser password. 


| name            | RAM | Ncores | use                                             | access                                                              | log in              |
|-----------------|-----|--------|-------------------------------------------------|---------------------------------------------------------------------|---------------------|
| ltesrv2.epfl.ch | 8 GB | 4   | Do not use for heavy computations !               | via ssh from EPFL domain (i.e. use VPN if not at EPFL)              | username or lteuser |
| ltesrv5.epfl.ch | 32 GB  |  12      |                                                 | via ssh from EPFL domain (i.e. use VPN if not at EPFL)              | username or lteuser |
| enacit-node01.epfl.ch  | 98 GB | 12     |                                                 | via ssh from EPFL domain (i.e. use VPN if not at EPFL)              | username or lteuser |
| enacit-node02.epfl.ch        | 98 GB | 12     |                                                 | via ssh from EPFL domain (i.e. use VPN if not at EPFL)              | username or lteuser |
| enacit-node09.epfl.ch        |     |        | Currently broken                                  | via ssh from EPFL domain (i.e. use VPN if not at EPFL)              | username or lteuser |
| enacit-node10.epfl.ch        |  24 GB | 24    |                                                 | via ssh from EPFL domain (i.e. use VPN if not at EPFL)              | username or lteuser |
| scitas          |     |        | https://www.epfl.ch/research/facilities/scitas/ |                                                                     |                     |
| CSCS            |     |        | MeteoSwiss research or GPU-heavy projects       | Access through MeteoSwiss or after acceptance of a proposal to CSCS |                     |

### Data repositories 

There exist different data repositories at LTE. Each data repository has different purposes and should be used accordingly.  
Data relevant to your work should be placed on `/ltenas3` or `/ltedata` depending on their relevance/reproducibility degree.

| Name       | Use                                   | Access                           | Log in            |
|------------|---------------------------------------|----------------------------------|-------------------|
| ltedata    | Irreproducible data                   | /ltedata                         | as lteuser        |
| ltenas3    | Downloadable data (model, reanalysis) | /ltenas3                         | as lteuser        |
| enacdrives | Lab documentation, lectures, various  | via program ENACdrives, ltefiles | personal username |
| gitlab     | Commonly used codes                   | gitlab.epfl.ch                   | personal account? |

###### ltedata
`/ltedata` is the main data repository and is mounted on almost all LTE computers (if not, ask Denis). 
It is backed-up frequently and therefore space on this disk is expensive: it should be used for irreproducible data (such as campaign data) or to save your software/projects scripts.

If you upload data to `/ltedata`
- Please upload them as `lteuser` and NOT with your personal username. This avoids to accidentally deleting data from your personal computer. 
- To update data as `lteuser`, you have 2 options:
  - Log-in as `lteuser` to one of the servers and pull the data from your computer 
  - Push the data directly from your computer terminal with: `rsync -azvh my_data_folder lteuser@ltesrv5.epfl.ch:/ltedata/`  
- Add a symbolic link to folders `/ltedata/0_Data`, `/ltedata/0_LTE_Campaigns`
- Campaigns naming conventions:
  - If spanning less than 1 year: <CAMPAIGN_NAME_IN_UPPERCASE>_<SINGLE_YEAR_YYYY_FORMAT>
  - If spanning more than 1 year: <CAMPAIGN_NAME_IN_UPPERCASE>_<YYYY_START>-<YYYY_END>
  
When creating a personal `<username>` folder into `/ltedata`: 
- Create the folder with your personal LTE username (in order to be able to modify/write what you put inside easily).
- Avoid to save data useful to other people of the lab in your personal user folder.
- When leaving the LTE, change the ownership of the folder to `lteuser` by typing `chown lteuser <your_folder_name>`. 
 
###### ltenas3
`/ltenas3` is also mounted on almost all LTE computers.
Storage space is comparatively cheaper than on `/ltedata`, but it is not fully backed-up. 
It should therefore be used for reproducible data such as model outputs, reanalysis, satellite data. 
There are no strict rules on how to manage data on `/ltenas3` but here are some best practice tips:
- Always write a README file in you data folder explaining what it contains and all necessary metadata.
- If possible, put the script (python/bash/...) necessary to download or create the data.
- Give at least read access to the group using `chmod g+r <my_data_folder>`
- Check the consistency of your data location with other existing repositories

###### enacdrives
[enacdrives](https://enacit.epfl.ch/enacdrives/) is an interactive tool developed by ENAC-IT for employees and students to facilitate access to storage services.
LTE uses the folder `commun1` mainly for documents and pictures.
For instance lectures, papers, conference talks, books are saved on `/commun1` in appropriate directories. 
Typically each field campaign has one folder on `/ltedata` and one on `/commun1` for documentation and pictures of the campaign. Here are some best practice tips:
- Upload your group meeting presentations, conference talks and posters to: `/commun1/Conferences/<year>/<conference_name>/<my_talk.pdf>`. 
- Upload your weather club presentations to: `/commun1/Weather_club/<weather_club_<NameOfPresenter>_<topic>_<yyyymmmdd.ppt>`
- Upload documentation and pictures of campaigns on `/commun1/Field_campaigns/<Campaign_name>`

###### GitLab
GitLab provides a Git-repository manager and EPFL hosts it as `gitlab.epfl.ch`.
LTE uses gitlab for common software codes and collaboration, such as `pyjacopo`, `pyWprof` and `masclab`.





### Computer set-up:

| Program       | Access                                                                               | Use                                                                                 | License |
|---------------|--------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------|---------|
| Anaconda      | https://docs.anaconda.com/anaconda/install/                                         | python environments                                                                 | free    |
| Matlab        | https://soft-epfl.epfl.ch/students/matlab/tah_en.cgi                                |                                                                                     | EPFL    |
| Spyder        | https://docs.spyder-ide.org/installation.html                                       | python IDE                                                                           | free    |
| Kite          | https://kite.com/                                                                   | python code assistant                                                               | free    |
| R             | https://cran.r-project.org/                                                          | R                                                                                   | free    |
| Studio        | https://rstudio.com/                                                                 | R IDE                                                                               | free    |
| Zoom          | https://zoom.us/                                                                   | Videoconferencing                                                                   | EPFL    |
| insync        | https://enacit.epfl.ch/backup-insync/                                               | File Backup                                                                         | EPFL    |
| EPFL printers | https://www.epfl.ch/campus/services/en/it-services/myprint/procedure/queue-install/  | Printing on big EPFL printers                                                       | -       |
| Overleaf      | https://www.overleaf.com/                                                        | Online latex                                                                         | EPFL    |
| Mendeley      | https://www.mendeley.com/                                                           | Literature management                                                               | free    |
| Zotero        | https://www.zotero.org/                                                              | Literature management                                                               | free    |
| GitHub        | https://github.com/                                                                 | Project version control, collaboration                                             | free    |
| GitLab        | https://gitlab.epfl.ch/                                                              | Project version control, collaboration. Hosted at EPFL, login with EPFL credentials | EPFL    |
| Libreoffice   | https://www.libreoffice.org/                                                        | Office programs                                                                     | free    |

### Programming guidelines:
- Matlab for MASC hydrometeor classification 
- R for statistical analysis
- Python for instrument data processing
- [Anaconda](https://docs.anaconda.com/anaconda/install/) for python programming environments
- [pyart](https://github.com/ARM-DOE/pyart) package for radar data processing
- [LTE GitLab](https://gitlab.epfl.ch/LTE) for frequently used code:  

### Suggestions for python and R:
- [Python TIPS](CodeSnippets/TIPS_R.md)
- [R TIPS](CodeSnippets/TIPS_Python.md)

### Leaving LTE: what to do?
First make sure to organise a really good apero, then follow these best practice tipps:
* Upload the codes that are useful for the lab to `gitlab.epfl.ch`. Make sure to document them.
* Clean and remove unnecessary data on all repositories.
* Change ownership (to `lteuser`) of your personal folders located on `/ltedata`and `/ltenas3`
using `chown lteuser <your_folder_name>` 

* Upload documents that are useful for the lab to `/commun1/<your_name>`, such as manuscript preparation, conference talks and others …
* Upload data you want to keep for the lab on either `/ltedata` or `/ltenas3`. Make sure to document them.

